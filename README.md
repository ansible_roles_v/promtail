Promtail
=========
Install and configure Promtail for Grafana Loki.  
Sometimes you need to add promtail service user into several groups.  
Specify them in the **promtail_groups** list variable. The first  
element of the list will be the main one.  

Include role
------------
```yaml
- name: promtail  
  src: https://gitlab.com/ansible_roles_v/promtail/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - promtail
```